# Pimp'UT

*Les MR sont les bienvenues, projet codé à la rache.*

## Téléchargement

### [Chrome & Chromium](https://chrome.google.com/webstore/detail/pimput/kjmmilemjflfhghfmdhfdfmdjiaaaihe?hl=fr)

### [Firefox & Firefox Android](https://random-utc.gitlab.utc.fr/Pimp-UT/pimp-ut--latest.xpi)

## Présentation

Pimp'UT : *demeter* en plus joli (et peut-être un jour avec plus de fonctionnaliés).
Extension pour navigateur, disponible sur [Chrome/Chromium](https://chrome.google.com/webstore/detail/pimput/kjmmilemjflfhghfmdhfdfmdjiaaaihe) et [Firefox](https://random-utc.gitlab.utc.fr/Pimp-UT/pimp-ut--latest.xpi)

**Ce que fait l'extension (elle est active uniquement sur les pages de l'UTC/demeter) :**
- Bloque le chargement des fichiers CSS de demeter ;
- Injecte une version modifiée du CSS de demeter ;
- Injecte un script Js qui permet de résoudre le problème des sauts de lignes (des pages de demeter ont la fâcheuse tendance à mettre des balises qu'il ne faut pas dans les balises HTML `<p>`, il a fallu faire un hack pour corriger le problème...).

**Aucune donnée n'est stockée ou transmise.**


## Dev.

### Déploiement d'une nouvelle version

⚠️⚠️ Merci de lire [cette page](https://gitlab.utc.fr/random-UTC/Pimp-UT/wikis/pages/Firefox-&-Chrome-:-mise-%C3%A0-jour-de-l'extension) du wiki. ⚠️⚠️

⚠️⚠️ NE JAMAIS CHANGER LA LOCALISATION DU FICHIER `app/updates/updates.json`, cela casserait la mise à jour automatique de Firefox ⚠️⚠️

### Install

```bash
npm install
```

### Development

```bash
npm run dev chrome
npm run dev firefox
npm run dev opera
npm run dev edge
```

### Build

```bash
npm run build chrome
npm run build firefox
npm run build opera
npm run build edge
```

### Environment

The build tool also defines a variable named `process.env.NODE_ENV` in your scripts.

### Docs

* [webextension-toolbox](https://github.com/HaNdTriX/webextension-toolbox)
