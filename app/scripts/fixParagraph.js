import $ from "jquery";

$(document).ready(() => {
  const allPs = document.getElementsByTagName("p");
  const re = /(<input)/g;
  Array.from(allPs).forEach(p => {
    if (re.test(p.innerHTML)) {
      p.classList.add("noPre");
    }
  });

  const allTds = document.getElementsByTagName('td');
  Array.from(allTds).forEach((td) => {
    td.innerHTML = td.innerHTML.replace('   ', '<br>').replace('    ', '<br>');
  })
});
