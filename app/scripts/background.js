import blocker from './blocker';

browser.runtime.onInstalled.addListener((details) => {
  console.log('previousVersion', details.previousVersion)
});

blocker();

browser.runtime.onMessage.addListener((request, sender) => {
  if (request.update) {
    handleUpdate(sender.tab.id);
  }
});

function isInDemeter(url) {
  let re = /(https:\/\/demeter\.utc\.fr)/g;
  return re.exec(url);
}

function handleUpdate(tabId) {
  browser.tabs.get(tabId).then((tab) => {

    const {url} = tab;
    if (isInDemeter(url)) {
      if (browser.tabs.removeCSS) {
        browser.tabs.removeCSS(
          tabId,
          {file: "/styles/custom.css", allFrames: true}
        )
      }
      browser.tabs.insertCSS(
        tabId,
        {file: "/styles/custom.css", allFrames: true}
      );
      browser.tabs.executeScript(
        tabId,
        {file: "/scripts/fixParagraph.js", allFrames: true}
      );
      browser.tabs.executeScript(
        tabId,
        {file: "/scripts/listenFrameChangeFirefox.js", frameId: 0}
      )

    }

  })
}

function handleSelectionChange(tabId) {
  browser.tabs.get(tabId).then((tab) => {
    browser.pageAction.hide(tabId);

    const {url} = tab;
    let iconPath = "images/icon-inactive-128.png";
    if (isInDemeter(url)) {
      iconPath = "images/icon-active-128.png";
      browser.pageAction.show(tabId);
    }
    browser.pageAction.setIcon(
      {tabId, path: iconPath}
    )
  })

}


browser.tabs.onUpdated.addListener((tabId) => {
  handleUpdate(tabId);
  handleSelectionChange(tabId);
});

browser.tabs.onActivated.addListener(tabInfo => {

  if (typeof tabInfo !== 'undefined') {
    handleSelectionChange(tabInfo.tabId);
  }
});
