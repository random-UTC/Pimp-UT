function blockRequest(details) {
  return {
    cancel: true
  };
}

export default function blocker() {
  if (browser.webRequest.onBeforeRequest.hasListener(blockRequest)) {
    return;
  }
  try {
    browser.webRequest.onBeforeRequest.addListener(blockRequest, {
      urls: [
        "https://demeter.utc.fr/*/CSS/COMMUN.CSS",
        "https://demeter.utc.fr/*/CSS/COMMUN_NEW.CSS",
        "https://demeter.utc.fr/*/CSS/commun.css"
      ]
    }, ['blocking']);
  } catch (e) {
    console.error("erreur", e);
  }
}