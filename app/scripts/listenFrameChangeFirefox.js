// Hack to listen to frame changes
// Firefox is not trigerring tab.onUpdated when frame content change

let frameset = document.getElementsByTagName('frameset');
if (frameset.length > 0) {
  if (!frameset[0].hasAttribute('already_listenning')) {

    document.addEventListener('DOMFrameContentLoaded', (e) => {
      browser.runtime.sendMessage({
        update: true
      });
    });
    frameset[0].setAttribute('already_listenning', true);
  }
}
