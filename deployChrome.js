var fs = require('fs');

// credentials and IDs from gitlab-ci.yml file (your appropriate config file)
let refreshToken = process.env.REFRESH_TOKEN,
  extensionId = process.env.EXTENSION_ID,
  clientSecret = process.env.CLIENT_SECRET,
  clientId = process.env.CLIENT_ID;


const webStore = require('chrome-webstore-upload')({
  extensionId,
  clientId,
  clientSecret,
  refreshToken
});

function findChromeZip(callback) {
  fs.readdir("./packages/", function (err, items) {
    const res = {fn: '', version: '0.0.0'}
    console.log(items);
    items.forEach(fn => {
      const re = /.*\.v([\d\.]+)\.(\w+)/g;
      const matches = re.exec(fn)
      const version = matches[1];
      const browser = matches[2];
      if (browser === 'chrome' && res.version < version) {
        res.fn = fn;
        res.version = version;
      }
    });
    callback('./packages/' + res.fn);
  });
}

function uploadZip(fn) {
  // creating file stream to upload
  const extensionSource = fs.createReadStream(fn);

  webStore.fetchToken().then(accessToken => {

    // upload the zip to webstore
    webStore.uploadExisting(extensionSource, accessToken).then(res => {
      console.log('Successfully uploaded the ZIP');

      // publish the uploaded zip
      webStore.publish("default", accessToken).then(res => {
        console.log('Successfully published the newer version');
      }).catch((error) => {
        console.error("Error while publishing uploaded extension", error);
        process.exit(1);
      });

    }).catch((error) => {
      console.error("Error while uploading ZIP, error");
      process.exit(1);
    });
  }).catch((error) => {
    console.error("Error while fetching token",  error);
    process.exit(1);
  });

}

findChromeZip((fn) => uploadZip(fn));
